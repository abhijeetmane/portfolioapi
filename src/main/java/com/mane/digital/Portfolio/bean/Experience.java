package com.mane.digital.Portfolio.bean;

import javax.persistence.*;

@Entity
@Table(name = "Experience")
public class Experience {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private Long id;
    private String title;
    private String company;
    private String location;
    private String fromYear;
    private String toYear;
    private String description;
    private String proudThing;

    @ManyToOne
    @JoinColumn(name = "profileId")
    private Profile profile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFromYear() {
        return fromYear;
    }

    public void setFromYear(String fromYear) {
        this.fromYear = fromYear;
    }

    public String getToYear() {
        return toYear;
    }

    public void setToYear(String toYear) {
        this.toYear = toYear;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProudThing() {
        return proudThing;
    }

    public void setProudThing(String proudThing) {
        this.proudThing = proudThing;
    }

    public Experience(){

    }

    public Experience(Long id, String title, String company, String location, String fromYear, String toYear, String description, String proudThing, Profile profile) {
        this.id = id;
        this.title = title;
        this.company = company;
        this.location = location;
        this.fromYear = fromYear;
        this.toYear = toYear;
        this.description = description;
        this.proudThing = proudThing;
    }
}
