package com.mane.digital.Portfolio.bean;

import javax.persistence.*;

@Entity
@Table(name = "Socials")
public class Socials {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "profileId")
    private Profile profile;

    private String name;
    private String sKey;
    private String link;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getsKey() {
        return sKey;
    }

    public void setsKey(String sKey) {
        this.sKey = sKey;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Socials(){}

    public Socials(Long id, String name, String sKey, String link) {
        this.id = id;
        this.name = name;
        this.sKey = sKey;
        this.link = link;
    }
}
