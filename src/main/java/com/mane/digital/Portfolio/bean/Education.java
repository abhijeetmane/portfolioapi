package com.mane.digital.Portfolio.bean;

import javax.persistence.*;

@Entity
@Table(name="Education")
public class Education {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "profileId")
    private Profile profile;

    private String university;
    private String fromYear;
    private String toYear;
    private String grade;
    private String degree;
    private String field;
    private String description;
    private String link;
    private String social;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getFromYear() {
        return fromYear;
    }

    public void setFromYear(String fromYear) {
        this.fromYear = fromYear;
    }

    public String getToYear() {
        return toYear;
    }

    public void setToYear(String toYear) {
        this.toYear = toYear;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    public Education(){

    }

    public Education(Long id, String university, String fromYear, String toYear, String grade, String degree, String field, String description, String link, String social) {
        this.id = id;
        this.university = university;
        this.fromYear = fromYear;
        this.toYear = toYear;
        this.grade = grade;
        this.degree = degree;
        this.field = field;
        this.description = description;
        this.link = link;
        this.social = social;
    }
}
