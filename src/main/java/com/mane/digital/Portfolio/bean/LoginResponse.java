package com.mane.digital.Portfolio.bean;


public class LoginResponse {

    private boolean loginSuccess;
    private String errorMsg;

    private LoginResponse(){

    }

    private LoginResponse(boolean loginSuccess, String errorMsg) {
        this.loginSuccess = loginSuccess;
        this.errorMsg = errorMsg;
    }

    public static LoginResponse createResponse(boolean loginSuccess, String errorMsg){
       return new LoginResponse(loginSuccess, errorMsg );
    }

    public boolean isLoginSuccess() {
        return loginSuccess;
    }

    public void setLoginSuccess(boolean loginSuccess) {
        this.loginSuccess = loginSuccess;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
