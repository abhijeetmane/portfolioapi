package com.mane.digital.Portfolio.bean;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Profile")
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private Long id;
    private String name;
    private String exp;
    private String dob;
    private String contact;
    private String emailId;
    private String country;
    private String address1;
    private String address2;
    private String address3;

    @OneToOne(mappedBy = "profile", cascade = CascadeType.ALL)
    private About about;

    @OneToMany(mappedBy = "profile", cascade = CascadeType.ALL)
    private List<Skills> skills;

    @OneToMany(mappedBy = "profile", cascade = CascadeType.ALL)
    private List<Experience> experiences;

    @OneToMany(mappedBy = "profile", cascade = CascadeType.ALL)
    private List<Education> educations;

    @OneToMany(mappedBy = "profile", cascade = CascadeType.ALL)
    private List<Socials> socials;


    public Profile(){

    }

    public Profile(Long id, String name, String exp,
                   String dob, String contact, String emailId,
                   String country, String address1, String address2,  String address3, About about, List<Skills> skills, List<Experience> experiences,
                   List<Education> educations, List<Socials> socials){
        this.id = id;
        this.name = name;
        this.exp = exp;
        this.dob = dob;
        this.contact = contact;
        this.emailId = emailId;
        this.country = country;
        this.address1 = address1;
        this.address2 = address2;
        this.address3 = address3;
        this.about = about;
        this.skills = skills;
        this.experiences = experiences;
        this.educations = educations;
        this.socials = socials;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getExp() {
        return exp;
    }

    public String getDob() {
        return dob;
    }

    public String getContact() {
        return contact;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getCountry() {
        return country;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getAddress3() {
        return address3;
    }

    public About getAbout() {
        return about;
    }

    public void setAbout(About about) {
        this.about = about;
    }

    public List<Skills> getSkills() {
        return skills;
    }

    public void setSkills(List<Skills> skills) {
        this.skills = skills;
    }

    public List<Experience> getExperiences() {
        return experiences;
    }

    public void setExperiences(List<Experience> experiences) {
        this.experiences = experiences;
    }

    public List<Education> getEducations() {
        return educations;
    }

    public void setEducations(List<Education> educations) {
        this.educations = educations;
    }

    public List<Socials> getSocials() {
        return socials;
    }

    public void setSocials(List<Socials> socials) {
        this.socials = socials;
    }
}
