package com.mane.digital.Portfolio.bean;

import javax.persistence.*;

@Entity
@Table(name = "Skills")
public class Skills {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private Long id;

    @ManyToOne
    @JoinColumn(name = "profileId")
    private Profile profile;

    private String skill;
    private String proficiency;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getProficiency() {
        return proficiency;
    }

    public void setProficiency(String proficiency) {
        this.proficiency = proficiency;
    }



    public Skills(){

    }

    public Skills(Long id, String skill, String proficiency) {
        this.id = id;
        this.skill = skill;
        this.proficiency = proficiency;
    }
}
