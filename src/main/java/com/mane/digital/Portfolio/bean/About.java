package com.mane.digital.Portfolio.bean;

import javax.persistence.*;

@Entity
@Table(name="About")
public class About {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private Long id;
    private String headline;
    private String cv;
    private String currentPosition;
    private String tagLine;
    private String description;
    private String summary;

    @OneToOne
    @JoinColumn(name = "profileId",nullable = false)
    private Profile profile;

    public String getHeadline() {
        return headline;
    }

    public String getCv() {
        return cv;
    }

    public String getCurrentPosition() {
        return currentPosition;
    }

    public String getTagLine() {
        return tagLine;
    }

    public String getDescription() {
        return description;
    }

    public String getSummary() {
        return summary;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public void setCurrentPosition(String currentPosition) {
        this.currentPosition = currentPosition;
    }

    public void setTagLine(String tagLine) {
        this.tagLine = tagLine;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

//    public void setProfile(Profile profile) {
//        this.profile = profile;
//    }

    public About(){

    }

    public About(Long id, String headline, String cv, String currentPosition,
                 String tagLine, String description, String summary){
        this.id = id;
        this.headline = headline;
        this.cv = cv;
        this.currentPosition = currentPosition;
        this.tagLine = tagLine;
        this.description = description;
        this.summary = summary;
    }

}
