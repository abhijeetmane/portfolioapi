package com.mane.digital.Portfolio.service;

import com.mane.digital.Portfolio.bean.About;
import com.mane.digital.Portfolio.repository.AboutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class AboutService implements IAboutService {

    @Autowired
    private AboutRepository aboutRepository;

    public About findAbout(Long profileId){
        return  aboutRepository.findById(profileId)
//                .orElseThrow(() -> new EntityNotFoundException("Profile not found for profile id"+ profileId));
                .orElse(null);
    }
}
