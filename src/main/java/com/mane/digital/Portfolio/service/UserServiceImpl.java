package com.mane.digital.Portfolio.service;

import com.mane.digital.Portfolio.bean.PasswordResetToken;
import com.mane.digital.Portfolio.bean.User;
import com.mane.digital.Portfolio.repository.PasswordResetTokenRepository;
import com.mane.digital.Portfolio.repository.RoleRepository;
import com.mane.digital.Portfolio.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class UserServiceImpl  implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Autowired
    private PasswordResetTokenRepository passwordTokenRepository;

    @Override
    public void save(User user) throws DataIntegrityViolationException {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public User findByUsername(String username){
        return userRepository.findByUsername(username);
    }

    @Override
    public void createPasswordResetTokenForUser(User user, String token) {
        PasswordResetToken existingToken = passwordTokenRepository.findByUserId(user.getId());
        if(existingToken != null){
           passwordTokenRepository.deleteById(existingToken.getId());
        }
        PasswordResetToken myToken = new PasswordResetToken(token, user);
        passwordTokenRepository.save(myToken);

    }

    @Override
    public void deletePasswordResetTokenForUser(User user, String token) {
        PasswordResetToken myToken = new PasswordResetToken(token, user);
        passwordTokenRepository.delete(myToken);
    }

    @Override
    public void changeUserPassword(User user, String password) {
        user.setPassword(bCryptPasswordEncoder.encode(password));
        userRepository.save(user);
    }
}
