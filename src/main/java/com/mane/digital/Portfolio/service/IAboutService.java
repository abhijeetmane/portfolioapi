package com.mane.digital.Portfolio.service;

import com.mane.digital.Portfolio.bean.About;

public interface IAboutService {
    About findAbout(Long profileId);
}
