package com.mane.digital.Portfolio.service;

import com.mane.digital.Portfolio.bean.Experience;

import java.util.List;

public interface IExperienceService {
    List<Experience> findExperiences(Long id);
}
