package com.mane.digital.Portfolio.service;

import com.mane.digital.Portfolio.bean.Skills;

import java.util.List;

public interface ISkillsService {
    List<Skills> findSkills(Long id);
}
