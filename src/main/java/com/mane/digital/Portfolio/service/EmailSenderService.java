package com.mane.digital.Portfolio.service;

import com.mane.digital.Portfolio.bean.ConfirmationToken;
import com.mane.digital.Portfolio.bean.User;
import com.mane.digital.Portfolio.repository.ConfirmationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderService {

    private JavaMailSender javaMailSender;

    @Autowired
    UserService userService;

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    public EmailSenderService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Async
    public boolean sendRegistrationEmail(User user) {
        ConfirmationToken confirmationToken = new ConfirmationToken(user);
        try {
            confirmationTokenRepository.save(confirmationToken);
            SimpleMailMessage mailMessage = composeRegistrationMail(user, confirmationToken);
            javaMailSender.send(mailMessage);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            confirmationTokenRepository.delete(confirmationToken);
            userService.delete(user);
            return false;
        }
    }

    @Async
    public void sendResetPasswordEmail(String username) {
        User user = getUserFromPrincipal(username);
        ConfirmationToken confirmationToken = new ConfirmationToken(user);
        try{
            userService.createPasswordResetTokenForUser(user, confirmationToken.getConfirmationToken());
            SimpleMailMessage mailMessage = composeResetPasswordMail(user, confirmationToken);
            javaMailSender.send(mailMessage);
        }catch (Exception e){
            e.printStackTrace();
            userService.deletePasswordResetTokenForUser(user, confirmationToken.getConfirmationToken());
        }
    }

    public SimpleMailMessage composeRegistrationMail(User user, ConfirmationToken confirmationToken){
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            String registrationLink = "http://localhost:5000/confirmAccount?token="+confirmationToken.getConfirmationToken();
            mailMessage.setTo(user.getUsername());
            mailMessage.setSubject("Complete Registration!");
            mailMessage.setFrom("maneabhijeet420@gmail.com");
            mailMessage.setText("To confirm your account, please click here : "+registrationLink);
            return mailMessage;
    }

    public SimpleMailMessage composeResetPasswordMail(User user, ConfirmationToken confirmationToken){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        String registrationLink = "http://localhost:5000/changePassword?id="+user.getId() +"&token="+confirmationToken.getConfirmationToken();
        mailMessage.setTo(user.getUsername());
        mailMessage.setSubject("Reset your password!");
        mailMessage.setFrom("maneabhijeet420@gmail.com");
        mailMessage.setText("To Reset your password, please click here : " +registrationLink);
        return mailMessage;
    }

    public User getUserFromPrincipal(String username){
        return userService.findByUsername(username);
    }

}

