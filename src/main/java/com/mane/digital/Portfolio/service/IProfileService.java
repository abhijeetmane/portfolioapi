package com.mane.digital.Portfolio.service;

import com.mane.digital.Portfolio.bean.About;
import com.mane.digital.Portfolio.bean.Profile;

public interface IProfileService {
    Profile findProfile(Long id);

    Profile setProfile(Profile profile);

    About setAbout(About about);
}
