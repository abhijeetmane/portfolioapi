package com.mane.digital.Portfolio.service;

import com.mane.digital.Portfolio.bean.Skills;
import com.mane.digital.Portfolio.repository.SkillsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class SkillsService implements ISkillsService{

    @Autowired
    private SkillsRepository SkillsRepository;

    public List<Skills> findSkills(Long profileId){
        return  SkillsRepository.findAllById(Arrays.asList(profileId));

//                .orElseThrow(() -> new EntityNotFoundException("Profile not found for profile id"+ profileId));

    }
}
