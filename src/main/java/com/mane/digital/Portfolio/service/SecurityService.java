package com.mane.digital.Portfolio.service;

public interface SecurityService {
    String findLoggedInUsername();

    void autoLogin(String username, String password);

    String validatePasswordResetToken(Long id, String token);
}
