
package com.mane.digital.Portfolio.service;

import com.mane.digital.Portfolio.bean.City;
import java.util.List;

public interface ICityService {

    List<City> findAll();
}