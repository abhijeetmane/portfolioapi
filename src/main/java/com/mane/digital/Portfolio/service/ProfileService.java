package com.mane.digital.Portfolio.service;

import com.mane.digital.Portfolio.bean.About;
import com.mane.digital.Portfolio.bean.Profile;
import com.mane.digital.Portfolio.repository.AboutRepository;
import com.mane.digital.Portfolio.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileService implements IProfileService {

    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private AboutRepository aboutRepository;


    public Profile findProfile(Long profileId){
        return  profileRepository.findById(profileId)
                .orElse(null);
    }

    public Profile setProfile(Profile profile){
        return profileRepository.save(profile);
    }

    public About setAbout(About about){
        return aboutRepository.save(about);
    }
}
