package com.mane.digital.Portfolio.service;

import com.mane.digital.Portfolio.bean.Experience;
import com.mane.digital.Portfolio.repository.ExperienceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class ExperienceService implements IExperienceService {


    @Autowired
    private ExperienceRepository experienceRepository;

    public List<Experience> findExperiences(Long profileId){
        return  experienceRepository.findAllById(Arrays.asList(profileId));

//                .orElseThrow(() -> new EntityNotFoundException("Profile not found for profile id"+ profileId));

    }
}
