package com.mane.digital.Portfolio.service;

import com.mane.digital.Portfolio.bean.User;

public interface UserService {
    void save(User user);

    void delete(User user);

    User findByUsername(String username);

    void createPasswordResetTokenForUser(User user, String token);

    void deletePasswordResetTokenForUser(User user, String token);

    void changeUserPassword(User user, String password);
}
