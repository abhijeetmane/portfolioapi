package com.mane.digital.Portfolio.repository;

import com.mane.digital.Portfolio.bean.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("UserMailRepository")
public interface UserMailRepository extends CrudRepository<User, String> {
    User findByUsernameIgnoreCase(String username);
}