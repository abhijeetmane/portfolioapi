package com.mane.digital.Portfolio.repository;

import com.mane.digital.Portfolio.bean.About;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AboutRepository extends JpaRepository<About, Long> {
}
