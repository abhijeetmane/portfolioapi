package com.mane.digital.Portfolio.repository;

import com.mane.digital.Portfolio.bean.Skills;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillsRepository extends JpaRepository<Skills, Long> {
}
