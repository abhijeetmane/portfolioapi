package com.mane.digital.Portfolio.repository;

import com.mane.digital.Portfolio.bean.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository  extends JpaRepository<Role, Long> {

}
