package com.mane.digital.Portfolio.repository;

import com.mane.digital.Portfolio.bean.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends CrudRepository<City, Long> {

}
