package com.mane.digital.Portfolio.repository;

import com.mane.digital.Portfolio.bean.Experience;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExperienceRepository extends JpaRepository<Experience, Long> {
}