package com.mane.digital.Portfolio.repository;

import com.mane.digital.Portfolio.bean.DBFile;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface DBFileRepository extends JpaRepository<DBFile, String> {

}