package com.mane.digital.Portfolio.repository;

import com.mane.digital.Portfolio.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
