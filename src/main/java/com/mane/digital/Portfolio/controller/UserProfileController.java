package com.mane.digital.Portfolio.controller;

import com.mane.digital.Portfolio.bean.*;
import com.mane.digital.Portfolio.repository.ConfirmationTokenRepository;
import com.mane.digital.Portfolio.repository.UserMailRepository;
import com.mane.digital.Portfolio.service.EmailSenderService;
import com.mane.digital.Portfolio.service.IProfileService;
import com.mane.digital.Portfolio.service.SecurityService;
import com.mane.digital.Portfolio.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserProfileController {

    @Autowired
    IProfileService profileService;

    @Autowired
    UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    private EmailSenderService emailSenderService;

    @Autowired
    private UserMailRepository userMailRepository;

    @GetMapping("/getProfile/{id}")
    public Profile getProfile(@PathVariable Long id){
       return profileService.findProfile(id);
    }

    @PostMapping("/profiles")
    public Profile setProfile(@RequestBody Profile profile) {
        return profileService.setProfile(profile);
    }

    @PostMapping("/profiles/about/{profileID}")
    public About setAbout(@RequestBody About about) {
        return profileService.setAbout(about);
    }

    @PostMapping("/userLogin")
    public LoginResponse login(@RequestBody User user){
        try{
            User currentUser = userService.findByUsername(user.getUsername());
            if(!currentUser.isEnabled()){
                return LoginResponse.createResponse(false,"Please activate your account by clicking on link sent to your email");
            }
            securityService.autoLogin(user.getUsername(), user.getPassword());
            return LoginResponse.createResponse(true,null);
        }catch (BadCredentialsException e){
            return LoginResponse.createResponse(false , "Invalid combination of username and password");
        }catch (NullPointerException e){
            return LoginResponse.createResponse(false , "Invalid combination of username and password");
        }catch (Exception e){
            return LoginResponse.createResponse(false , "Invalid combination of username and password");
        }
    }

    @PostMapping("/userRegister")
    public LoginResponse register(@RequestBody User user){
        try{
            User newUser = userService.findByUsername(user.getUsername());
            if(null==newUser){
                userService.save(user);
                if(!emailSenderService.sendRegistrationEmail(user)){
                    return LoginResponse.createResponse(false,"Problem occurred!!! Please try again later");
                }
                return LoginResponse.createResponse(true,"Account verification link has been sent to your email id");
            }
          return LoginResponse.createResponse(false,"Username is already taken");
        }catch (DataIntegrityViolationException e){
            return LoginResponse.createResponse(false , "Username is already taken");
        }catch (Exception e){
            return LoginResponse.createResponse(false , "Problem occurred!!! Please try again later");
        }
    }

    @GetMapping("/confirmAccount")
    public LoginResponse confirmUserAccount(@RequestParam("token") String confirmationToken) {
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
        if(token != null) {
            User user = userService.findByUsername(token.getUser().getUsername());
            user.setEnabled(true);
            userMailRepository.save(user);
            return LoginResponse.createResponse(true,"Registration is successful");
        }
        return LoginResponse.createResponse(false,"The link is invalid or broken!");
    }

    @PostMapping("/resetPassword")
    public LoginResponse resetPassword(@RequestParam("email") String userEmail) {
        User user = userService.findByUsername(userEmail);
        if (user == null) {
            return LoginResponse.createResponse(false, "User does not exist");
        }
        emailSenderService.sendResetPasswordEmail(userEmail);
        return LoginResponse.createResponse(false, "Password reset link is sent to your email id");
    }

    @GetMapping("/changePassword")
    public LoginResponse showChangePasswordPage(@RequestParam("id") long id, @RequestParam("token") String token) {
        String result = securityService.validatePasswordResetToken(id, token);
        if (result != null) {
            return LoginResponse.createResponse(false, "Password reset link is not valid.");
        }
        return LoginResponse.createResponse(true, "Password reset is in progress.");
    }


    @PostMapping("/savePassword")
    public LoginResponse savePassword(@RequestBody User paramsUser) {
        User user =(User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userService.changeUserPassword(user, paramsUser.getPassword());
        return LoginResponse.createResponse(true, "Password changed successfully");
    }
}