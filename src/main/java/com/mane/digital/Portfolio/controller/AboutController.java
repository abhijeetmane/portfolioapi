package com.mane.digital.Portfolio.controller;

import com.mane.digital.Portfolio.bean.About;
import com.mane.digital.Portfolio.service.IAboutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class AboutController {

    @Autowired
    IAboutService aboutService;

    @GetMapping("/getAbout/{id}")
    public HashMap<String, Object> getAbout(@PathVariable Long id){
        About about = aboutService.findAbout(id);
        HashMap<String, Object> aboutData = new HashMap<>();
        aboutData.put("about",about);
        return aboutData;
    }
}
