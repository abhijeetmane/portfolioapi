package com.mane.digital.Portfolio.controller;

import com.mane.digital.Portfolio.bean.City;
import com.mane.digital.Portfolio.service.ICityService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;

@RestController
public class MyController {

    @Autowired
    ICityService cityService;

    @GetMapping("/showCities")
    public List<City> findCities(Model model) {

        List<City> cities = cityService.findAll();

        model.addAttribute("cities", cities);

        String citiesJson = new Gson().toJson(cities);

        return cities;
    }
}